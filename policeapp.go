package policeapp

import (
	"fmt"
  "os"
	_ "github.com/lib/pq"
	"bitbucket.org/jzmine/policeapp/models"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/astaxie/beego/cache"
	_ "github.com/astaxie/beego/cache/redis"
  "github.com/speps/go-hashids"
  "bitbucket.org/jzmine/policeapp/models/rbac"
)

const HASH_ID_SALT  = "9ae8f6b5e6c4e6c83dced6e4cb289ab8"
var (
  HashId *hashids.HashID
	PoliceCache cache.Cache
)

func init() {
	setupDB()
	setupCache()
  setupOthers()
	rbac.SetupRbac()
	println("package police_app init!")
}

func setupDB()  {
  var dbParams string
  println("OS-env:  " + os.Getenv("OPENSHIFT_GEAR_NAME"))
  if dbname := os.Getenv("OPENSHIFT_GEAR_NAME"); dbname == "" {
    dbParams = "user=postgres password=jasmine dbname=police_crime_db sslmode=disable port=5433"
  } else {
    dbParams = fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable host=/tmp port=%s",
      os.Getenv("OPENSHIFT_PG_DB_USERNAME"), os.Getenv("OPENSHIFT_PG_DB_PASSWORD"),  os.Getenv("OPENSHIFT_APP_NAME"),
      /*os.Getenv("OPENSHIFT_PG_HOST"), */os.Getenv("OPENSHIFT_PG_PRIVATE_PORT"))
  }
  //println(dbParams)

  var err error
	//dbParams := "user=postgres password=jasmine dbname=police_crime_db sslmode=disable port=5433"
	if models.DBOrm, err = gorm.Open("postgres", dbParams); err != nil {
		panic(err)
	}
	if err = models.DBOrm.DB().Ping(); err != nil {
		panic(err)
	}
	if err == nil {
		models.DBOrm.LogMode(true)
		models.DBOrm.DB().SetMaxIdleConns(10)
		models.DBOrm.DB().SetMaxOpenConns(100)
		models.DBOrm.SingularTable(true)
	}
	println("DB Setup")
}

func setupCache() {
	rediscon := fmt.Sprintf("%s", os.Getenv("OPENSHIFT_REDIS_HOST"))
	if rediscon == "" {
		rediscon = `{"key":"policeCache","conn": "127.0.0.1:6379"}`
	} else {
		rediscon = fmt.Sprintf(`{"key":"policeCache","conn":"%s:%s", "password": "%s"}`,rediscon, os.Getenv("OPENSHIFT_REDIS_PORT"),
			os.Getenv("REDIS_PASSWORD"))
	}

	var err error
  PoliceCache, err = cache.NewCache("redis", rediscon)
  if(err != nil) {
    panic(err)
  }
  println("Police Cache ready!")
}

func setupOthers()  {
  hd := hashids.NewData()
  hd.Salt = HASH_ID_SALT
  hd.MinLength = 30
  HashId = hashids.NewWithData(hd)
}

func Testing() {
	println("Testing called28!")
}