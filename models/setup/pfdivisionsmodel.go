package setup

import (
	"bitbucket.org/jzmine/policeapp/models"
	"github.com/jinzhu/gorm"
	"bitbucket.org/jzmine/go_myutils"
	"bitbucket.org/jzmine/policeapp/models/lookups"
)

type PFDivisions struct {
	Id int64 `gorm:"column:pf_divisions_id;primary_key" json-"-"`
	DivisionName string
	PFAreaCommandsId int64
	LgaId int64
	Address string
	models.Model
}

func (ac *PFDivisions) BeforeCreate(scope *gorm.Scope) {
	scope.SetColumn("Uid", go_myutils.GetUUIDMD5Hash())
}

type DTOPFDivisions struct {
	AreaCommandName string
	LgaName string
	DivisionName string
	Active bool
	Address string
	Uid string
}

func DTOPFDivisionsGetAll(acn string) ([]DTOPFDivisions, error) {
	var dtos []DTOPFDivisions
	db := models.DBOrm.Table("vw_division_lga_area_state v").
		Select("v.area_command_name, v.lga_name, v.division_name, v.uid, v.active, v.address")
	if acn != "" {
		db = db.Where("area_command_name = ?", acn)
	}
	if err := db.Order(" v.lga_name, v.division_name").Scan(&dtos).Error; err != nil {
		return nil, err
	}
	return dtos, nil
}

func DTOPFDivisionsCRUD(action string, dto *DTOPFDivisions) (error)   {
	var err error
	var fd PFDivisions
	if action != "insert" {
		fd.Id, err = models.GetPkValue(&PFDivisions{}, dto.Uid)
		if(err != nil) {return err}
	}
	if action != "remove" {
		fd.DivisionName = dto.DivisionName
		fd.PFAreaCommandsId, err = models.GetPkValueByField(&PFAreaCommands{},"area_command_name", dto.AreaCommandName)
		if(err != nil) {return err}
		fd.LgaId, err = models.GetPkValueByField(&lookups.Lga{}, "lga_name", dto.LgaName)
		if(err != nil) {return err}
		fd.Active = dto.Active
		fd.Address = dto.Address
		if action == "insert" {
			fd.Active = dto.Active
			fd.DivisionName = dto.DivisionName
			err = models.DBOrm.Save(&fd).Error
		} else {
			err = models.DBOrm.Model(&fd).Update(map[string]interface{}{"DivisionName":dto.DivisionName, "Active":dto.Active,
				"Address": dto.Address, "LgaId": fd.LgaId}).Error
		}
	} else {
		err = models.DBOrm.Delete(&fd).Error
	}

	return err
}

func JsonArrayStatesWithAreaCommands() (data []map[string]interface{},  err error)  {
	rows, err := models.DBOrm.Raw(`SELECT distinct d.state_name
		from vw_division_lga_area_state d where d.area_command_name is not null order by d.state_name`).Rows()
	defer rows.Close()
	for rows.Next() {
		var statename string
		rows.Scan(&statename)

		var jac []interface{}
		var acname string
		/*rowsAC, _ := models.DBOrm.Table() Raw(fmt.Sprintf(`SELECT distinct d.area_command_name
			from vw_division_lga_area_state d where state_name = '%s' and area_command_name is not null
		`, statename)).Rows()*/
		rowsAC, _ := models.DBOrm.Table("vw_division_lga_area_state").Select("distinct area_command_name").
			Where("state_name = ?  and area_command_name is not null", statename).Order("area_command_name").Rows()
		defer rowsAC.Close()
		for rowsAC.Next() {
			rowsAC.Scan(&acname)
			jac = append(jac, acname)
		}
		//jsState := map[string]interface{}{"statename":statename}
		data = append(data, map[string]interface{}{"statename":statename, "areacommands":jac})
	}
	return
}

func JsonArrayGetAreaCommandDivisionNames(acn string) (data []map[string]interface{}) {
	rows, _ := models.DBOrm.Table("vw_division_lga_area_state").Select("division_name").
		Where("area_command_name = ?", acn).Order("division_name").Rows()
	defer rows.Close()
	var lganame string
	for rows.Next() {
		rows.Scan(&lganame)
		data = append(data, map[string]interface{}{"DivisionName":lganame})
	}
	return
}