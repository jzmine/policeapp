package setup

import (
	"bitbucket.org/jzmine/policeapp/models"
	//"github.com/jinzhu/gorm"
	"bitbucket.org/jzmine/policeapp/models/tables"
	"database/sql"
	"bitbucket.org/jzmine/policeapp/models/lookups"
  "encoding/json"
)

type DTOFormationPoliceOfficers struct {
	Uid string `gorm:"primary_key"`
	Surname, Firstname, Othernames string
	Sex string `gorm:"column:birth_sex"`
	RankName string
	PfType string
	PfTypeId int64
	StateName string //`json:"-"`
	Active bool
	FormationType, FormationName string
}

func DTOFormationPoliceOfficersGetAll(stateName, formaType, formaName string) (data []DTOFormationPoliceOfficers, err error)  {
	err = models.DBOrm.
		Where("state_name = ? and formation_type = ? and formation_name = ? and deleted_at is null", stateName, formaType, formaName).
		Find(&data).Error
	return
}

func JsonArrayDropdownDTOPoliceOfficersGetAll(stateName, formaType, formaName string) (data []map[string]interface{}, err error)  {
	var sjson sql.NullString
  row := models.DBOrm.Raw(`SELECT json_agg(jsb.json_build_object) from (
  	select json_build_object('text', f.fullnames, 'rank',f.rank_name, 'sex',
	  case f.birth_sex		when 'M' then 'Male' when 'F' then 'Female' else 'Unknown' end )
    from dto_formation_police_officers f where f.state_name = ? and f.formation_type = ? and f.formation_name = ?
    order by f.fullnames  ) jsb `, stateName, formaType, formaName).Row()
  err = row.Scan(&sjson)
  if err == nil && sjson.Valid {
    err = json.Unmarshal([]byte(sjson.String), &data)
  }
  return
}

func DTOFormationPoliceOfficersCRUD(action string, dto *DTOFormationPoliceOfficers) (error)   {
	var err error
	var p tables.Persons
	var po tables.PoliceOfficers
  var ent tables.Entities
	if action != "insert" {
		//var id int64
		//id, err = models.GetPkValue(&tables.Entities{}, dto.Uid)
		//if(err != nil) {return err}
		//models.DBOrm.Find(&p, id).Related(&po)
    err = models.DBOrm.Find(&ent, "uid = ?", dto.Uid).Error
    if(err != nil) {return err}
		//err = models.DBOrm.Find(&p, "entities_id = ?", ent.Id).Error
    if(err != nil) {return err}
	}
	tx := models.DBOrm.Begin()
	if action != "remove" {
		if(err != nil) {return err}
		po.PfType, po.PfTypeId, err =	lookups.GetFormationCodeId(dto.StateName, dto.FormationType, dto.FormationName)
		if err != nil {
			tx.Rollback()
			return err
		}
		po.PoliceRanksId, err = models.GetPkValueByField(&lookups.PfRanks{},"rank_name", dto.RankName)
		if err != nil {
			tx.Rollback()
			return err
		}
		if action == "insert" {
      ent := tables.Entities{
        Model: models.Model{Active:dto.Active},
        EntityType: string('p'),
				Persons: tables.Persons{
					Othernames: sql.NullString{String:dto.Othernames, Valid: true},
					Firstname: dto.Firstname,
					BirthSex: dto.Sex[0:1],
					Surname: dto.Surname,
					Bvn: sql.NullString{String:"NA",Valid:true},
				},
        PoliceOfficers: tables.PoliceOfficers{
          PfTypeId:po.PfTypeId,
          PfType:po.PfType,
          PoliceRanksId:po.PoliceRanksId,
        },
      }
      err = tx.Create(&ent).Error
      if err != nil {
        tx.Rollback()
        return err
      }
			/*p = tables.Persons{
        Id: ent.Id,
				Othernames: sql.NullString{String:dto.Othernames, Valid: true},
				Firstname: dto.Firstname,
				BirthSex: dto.Sex[0:1],
				Surname: dto.Surname,
				Bvn: sql.NullString{String:"NA",Valid:true},
			}*/
			/*err = tx.Create(&p).Error
			if err != nil {
				tx.Rollback()
				return err
			}*/
			/*po = tables.PoliceOfficers{PfTypeId:po.PfTypeId, PfType:po.PfType,PoliceRanksId:po.PoliceRanksId, EntitiesId:p.Id}
			err = tx.Create(&po).Error
			if err != nil {
				tx.Rollback()
				return err
			}*/
      dto.Uid = ent.Uid
		} else {
      ent.Model.Active = dto.Active
      /*ent.Persons.Surname = dto.Surname
      ent.Persons.Firstname = dto.Firstname
      ent.Persons.Othernames = sql.NullString{String:dto.Othernames, Valid: true}
      ent.Persons.BirthSex = dto.Sex[0:1]*/
      //ent.PoliceOfficers.PfType = po.PfType
      //ent.PoliceOfficers.PfTypeId = po.PfTypeId
      //ent.PoliceOfficers.PoliceRanksId = po.PoliceRanksId

      err = tx.Model(&ent).Update("active", dto.Active).Error
      //err = tx.Save(&ent).Error
      if err == nil {
        err = tx.Model(&p).Where("entities_id = ?", ent.Id).
					Update(map[string]interface{}{"Surname":dto.Surname, "Firstname":dto.Firstname,"Othernames":dto.Othernames,
					"Active":dto.Active, "BirthSex":dto.Sex[0:1]}).Error
      }
      if err != nil {
        tx.Rollback()
        return err
      }
			//update person
			/*err = tx.Model(&p).Update(map[string]interface{}{"Surname":dto.Surname, "Firstname":dto.Firstname,
				"Othernames":dto.Othernames, "Active":dto.Active, "BirthSex":dto.Sex[0:1]}).Error
			if err != nil {
				tx.Rollback()
				return err
			}
			po.EntitiesId = p.Id
			err = tx.Save(&po).Error
			if err != nil {
				tx.Rollback()
				return err
			}*/
			//err = models.DBOrm.Model(&po).upda
			//err = models.DBOrm.Model(&pp).Update(map[string]interface{}{"PolicePostName":dto.PolicePostName, "Active":dto.Active,
			//	"Address": dto.Address, "PFDivisionsId": pp.PFDivisionsId}).Error
		}
	} else {
		err = tx.Delete(&ent).Error
	}
	//tx.Rollback()
	tx.Commit()
	return err
}