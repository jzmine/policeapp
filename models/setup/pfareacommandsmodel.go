package setup

import (
	"bitbucket.org/jzmine/policeapp/models"
	"bitbucket.org/jzmine/go_myutils"
	"github.com/jinzhu/gorm"
	"errors"
)


type PFAreaCommands struct {
	Id int64 `gorm:"column:pf_area_commands_id;primary_key" json:"-" `
	AreaCommandName string
	Address string
	PfStateCommandsId int64
	ConstituencyId int64
	models.Model
}

func (ac *PFAreaCommands) BeforeCreate(scope *gorm.Scope) {
	scope.SetColumn("Uid", go_myutils.GetUUIDMD5Hash())
}

type DTOAreaCommands struct {
	Uid string
	ConstituencyName string
	AreaCommandName string
	Address string
	Active bool
	StateCommandName string
}

func DTOAreaCommandsGetAll(statename string) ([]DTOAreaCommands, error) {
	var dtos []DTOAreaCommands
	db := models.DBOrm.Table("vw_area_state_commands v").
		Select("v.area_command_name, v.uid, v.active, v.address, v.constituency_name")
	if statename != "" {
		db = db.Where("state_name = ?", statename)
	}
	if err := db.Order("v.constituency_name, v.area_command_name").Scan(&dtos).Error; err != nil {
		return nil, err
	}
	return dtos, nil
}


func DTOAreaCommandsCRUD(action string, dto *DTOAreaCommands) (error)   {
	/*{"value":{"Uid":"3e0f452695862df4548298ac35b1d09d","LGAName":"ABA NORTH","AreaCommandName":null,"Active":true,"Address":null,"ejControl_816":"edit","ejControl_817":"save","ejControl_818":"cancel"},"action":"update","keyColumn":"Uid","key":"3e0f452695862df4548298ac35b1d09d","table":null,"params":{}}*/
	var err error
	if action == "insert" {
		return errors.New("Cannot insert record!")
	}
	var ac PFAreaCommands
	if action != "insert" {
		ac.Id, err = models.GetPkValue(&PFAreaCommands{}, dto.Uid)
	}
	if action != "remove" {
		/*ac.Fullnames = dto.AreaCommandName
		ac.Active = dto.Active
		ac.Address = dto.Address*/
		err = models.DBOrm.Model(&ac).Update(map[string]interface{}{"AreaCommandName":dto.AreaCommandName,"Active":dto.Active,
			"Address": dto.Address}).Error
	} else {
		err = models.DBOrm.Delete(&ac).Error
	}

	return err
}

func GetAreaCommandStatename(acn string) (statename string)  {
	row := models.DBOrm.Table("vw_area_state_commands").Select("state_name").Where("area_command_name = ?",acn).Row()
	row.Scan(&statename)
	return
}