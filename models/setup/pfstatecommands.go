package setup

import (
	"bitbucket.org/jzmine/policeapp/models"
	"bitbucket.org/jzmine/go_myutils"
	"github.com/jinzhu/gorm"
	"bitbucket.org/jzmine/policeapp/models/lookups"
)


type PfStateCommands struct {
	Id int64 `gorm:"column:pf_state_commands_id;primary_key" json:"-" `
	Fullnames string
	NigStateId int64
	Uid string
	//Active bool
	Address string
	models.Model
}

func (fc *PfStateCommands) BeforeCreate(scope *gorm.Scope) {
	scope.SetColumn("Uid", go_myutils.GetUUIDMD5Hash())
}

func GetStateStateCommandName(statename string) (scn string) {
	//err := nil
	models.DBOrm.Table("vw_states_state_command").Select("state_command_name").Where("state_name = ?", statename).
		Row().Scan(&scn)
	return
}

type DTOPFStateCommands struct {
	Fullnames string
	Statename string
	Uid string
	Active bool
	Address string
}

func DTOPFStateCommandsGetAll() ([]DTOPFStateCommands, error) {
	var dtos []DTOPFStateCommands
	if err := models.DBOrm.Table("pf_state_commands psh").Select("psh.fullnames, psh.uid, psh.active, psh.address, ns.state_name as statename").
		Joins("left join nig_state ns on psh.nig_state_id = ns.nig_state_id").Order("ns.state_name").Scan(&dtos).Error; err != nil {
		return nil, err
	}
	return dtos, nil
}

func JsonArrayStatesWithStateCommands() (data []map[string]interface{},  err error)  {
	rows, err := models.DBOrm.Raw(`select distinct state_name from vw_area_state_commands
		where state_command_name is not null order by state_name`).Rows()
	defer rows.Close()
	for rows.Next() {
		var statename string
		rows.Scan(&statename)
		data = append(data, map[string]interface{}{"statename":statename})
	}
	return
}

func DTOPFStateCommandsCRUD(action string, dto *DTOPFStateCommands) (error)   {
	/*{"value":{"Uid":"8640875bf106bfba9e8e2f09b2c03666","PFCUid":"11542d8a56decd28d6a867acd42952db","PFCUid_hidden":"Speaking","Feature":"Slurped Speech","Active":true,"Active_hidden":"Yes","ejControl_600":"edit","ejControl_601":"delete","ejControl_602":"save","ejControl_603":"cancel"},"action":"update","keyColumn":"Uid","key":"8640875bf106bfba9e8e2f09b2c03666","table":null,"params":{}}*/
	var err error
	var shq PfStateCommands
	if action != "insert" {
		shq.Id, err = models.GetPkValue(&PfStateCommands{}, dto.Uid)
	}
	if action != "remove" {
		shq.Fullnames = dto.Fullnames
		shq.Active = dto.Active
		shq.Address = dto.Address
		shq.NigStateId, err = models.GetPkValueByField(&lookups.NigState{}, "state_name", dto.Statename)
		if(err != nil) {return err}
		if action == "insert" {
			err = models.DBOrm.Create(&shq).Error
		} else {
			err = models.DBOrm.Model(&shq).Update(map[string]interface{}{"Fullnames":dto.Fullnames,"Active":dto.Active,
				"NigStateId":shq.NigStateId, "Address": dto.Address}).Error
		}
	} else {
		err = models.DBOrm.Delete(&shq).Error
	}

	return err
}
