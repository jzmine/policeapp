package setup

import (
	"bitbucket.org/jzmine/policeapp/models"
	"github.com/jinzhu/gorm"
	"bitbucket.org/jzmine/go_myutils"
	//"bitbucket.org/jzmine/policeapp/models/lookups"
)

type PFPolicePosts struct {
	Id int64 `gorm:"column:pf_police_posts_id;primary_key" json-"-"`
	PolicePostName string
	PFDivisionsId int64
	Address string
	models.Model
}

func (pp *PFPolicePosts) BeforeCreate(scope *gorm.Scope) {
	scope.SetColumn("Uid", go_myutils.GetUUIDMD5Hash())
}

type DTOPFPolicePosts struct {
	PolicePostName string
	DivisionName string
	Active bool
	Address string
	Uid string
}

func DTOPFPolicePostsGetAll(acn string) ([]DTOPFPolicePosts, error) {
	var dtos []DTOPFPolicePosts
	db := models.DBOrm.Table("vw_police_post_state_area_division v").
		Select("v.police_post_name, v.division_name, v.uid, v.active, v.address")
	if acn != "" {
		db = db.Where("area_command_name = ? and police_post_name is not null", acn)
	}
	if err := db.Order(" v.division_name, v.police_post_name").Scan(&dtos).Error; err != nil {
		return nil, err
	}
	return dtos, nil
}

func DTOPFPolicePostsCRUD(action string, dto *DTOPFPolicePosts) (error)   {
	var err error
	var pp PFPolicePosts
	if action != "insert" {
		pp.Id, err = models.GetPkValue(&PFPolicePosts{}, dto.Uid)
		if(err != nil) {return err}
	}
	if action != "remove" {
		pp.PFDivisionsId, err = models.GetPkValueByField(&PFDivisions{},"division_name", dto.DivisionName)
		if(err != nil) {return err}
		if action == "insert" {
			pp.PolicePostName = dto.PolicePostName
			pp.Active = dto.Active
			pp.Address = dto.Address
			err = models.DBOrm.Save(&pp).Error
		} else {
			err = models.DBOrm.Model(&pp).Update(map[string]interface{}{"PolicePostName":dto.PolicePostName, "Active":dto.Active,
				"Address": dto.Address, "PFDivisionsId": pp.PFDivisionsId}).Error
		}
	} else {
		err = models.DBOrm.Delete(&pp).Error
	}

	return err
}

func JsonArrayStatesWithAreaCommandsDivisions() (data []map[string]interface{},  err error) {
	rows, err := models.DBOrm.Raw(`SELECT distinct d.state_name
		from vw_police_post_state_area_division d where d.division_name is not null order by d.state_name`).Rows()
	defer rows.Close()
	for rows.Next() {
		var statename string
		rows.Scan(&statename)

		var jac []map[string]interface{}
		var acname string

		rowsAC, _ := models.DBOrm.Table("vw_police_post_state_area_division").Select("distinct area_command_name").
			Where("state_name = ?  and area_command_name is not null", statename).Order("area_command_name").Rows()
		defer rowsAC.Close()
		for rowsAC.Next() {
			rowsAC.Scan(&acname)

			rowsD, _ := models.DBOrm.Table("vw_police_post_state_area_division").Select("distinct division_name").
				Where("area_command_name = ?  and division_name is not null", acname).Order("division_name").Rows()
			defer rowsD.Close()

			var dname string
			var jd []string
			for rowsD.Next() {
				rowsD.Scan(&dname)
				jd = append(jd, dname)
			}
			jac = append(jac, map[string]interface{}{"areacommand": acname, "divisions":jd})
		}

		data = append(data, map[string]interface{}{"statename":statename, "areacommands":jac})
	}
	return

}