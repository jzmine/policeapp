package security

import (
  "math/rand"
  "time"
  "bitbucket.org/jzmine/policeapp"
  "bitbucket.org/jzmine/policeapp/models"
  "github.com/jinzhu/gorm"
  "github.com/elithrar/simple-scrypt"
  "bitbucket.org/jzmine/go_myutils"
  "errors"
)

type Users struct {
  Id int64 `gorm:"column:users_id;primary_key"`
  Username string
  Password string `sql:"-"`
  Fullnames string
  PfTypeId int64
  PfType string
  PasswordHash string `json:"-"`
  models.Model
}

func (u *Users) BeforeCreate(scope *gorm.Scope)(err error) {
  scope.SetColumn("Uid", go_myutils.GetUUIDMD5Hash())
  if(u.Password != "") {
    var hash []byte
    hash, err = scrypt.GenerateFromPassword([]byte(u.Password), scrypt.DefaultParams)
    if err == nil {
      scope.SetColumn("PasswordHash", hash)
      err = nil
    }
  }
  return
}

type DTOUser struct {
  Uid string
  Username string
  Fullnames string
  Roles []string
}

func ValidateUser(username, password string) (dto *DTOUser, token string,  err error) {
  var luser Users
  err = models.DBOrm.Where("username = ?", username).Find(&luser).Error
  if(err != nil) {
    return nil, "", errors.New("Invalid Username")
  }
  if err = scrypt.CompareHashAndPassword([]byte(luser.PasswordHash), []byte(password)); err != nil {
    return nil, "", errors.New("Invalid Password")
  }

  dto = &DTOUser{
    Uid:luser.Uid,
    Username:username,
    Fullnames:luser.Fullnames,
  }

  //get his roles
  models.DBOrm.Raw(`select r.role_name from users u join users_rbac_roles ur on u.users_id = ur.users_id
    join rbac_roles r on ur.rbac_roles_id = r.rbac_roles_id where u.users_id = ?`, luser.Id).Pluck("role_name", &dto.Roles)

  r := rand.New(rand.NewSource(time.Now().UnixNano()))
  token, _ = policeapp.HashId.Encode([]int{r.Int()})
  return dto, token, nil
}