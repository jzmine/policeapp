package lookups

import (
	"bitbucket.org/jzmine/policeapp/models"
	"encoding/json"
)

type State struct {
	Id int64 `db:"nig_state_id" json:"-"`
	Name string `db:"state_name"`
	Uid string `db:"uid"`
}

func GetFormationsData()(data []map[string]interface{}, err error)  {
	var sjson string
	row := models.DBOrm.Raw(`SELECT json_agg(json_build_object('state_name',state_name, 'state_command_name',COALESCE(state_command_name, ''),
		'area_command_name', COALESCE(area_command_name,''), 'division_name', COALESCE(division_name,''), 'police_post_name',
		COALESCE(police_post_name, '')))
		from vw_police_post_state_area_division
		where state_command_name is not null or area_command_name is not null or  division_name is not null or  police_post_name is not null`).
		Row()
	row.Scan(&sjson)
	json.Unmarshal([]byte(sjson), &data)
	return
}

func GetFormationCodeId(statename, formationtype, formationname string) (formationCode string, formationId int64, err error)  {
	formationCode = formationtype[0:1]
	var fieldname string
	db := models.DBOrm
	if formationCode == "s" {
		fieldname = "pf_state_commands_id"
		db = db.Where("state_command_name = ? and state_name = ?", formationname, statename)
	} else if formationCode == "d" {
		fieldname = "pf_divisions_id"
		db = db.Where("division_name = ? and state_name = ?", formationname, statename)
	} else if formationCode == "a" {
		fieldname = "pf_area_commands_id"
		db = db.Where("area_command_name = ? and state_name = ?", formationname, statename)
	} else {
		fieldname = "pf_police_posts_id"
		db = db.Where("police_post_name = ? and state_name = ?", formationname, statename)
	}
	row := db.Table("vw_police_post_state_area_division").Select(fieldname).Row()
	err = row.Scan(&formationId)
	return
}