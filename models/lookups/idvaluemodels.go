package lookups

import (
  "bitbucket.org/jzmine/policeapp/models"
  "bitbucket.org/jzmine/go_myutils"
  "github.com/jinzhu/gorm"
  "encoding/json"
  "errors"
  "time"
)
var ErrInvalidLookupSettings = errors.New("Invalid lookup settings!")

type CaseCategory struct {
  Id int64 `gorm:"column:case_category_id;primary_key" json:"-"`
  Category string
  models.Model
}

type DTOIdValue struct {
  Value string
  Uid string
  Active bool
  Tbl string
}

func GetDTOIdValuesGetAll(tblname string) (data []DTOIdValue, title, valueheader string, err error) {
  var sjson string
  if tblname == "cas_cat" {
    row := models.DBOrm.Raw(`select json_agg(jbo.json_build_object) from (
      select json_build_object('Uid', cc.uid, 'Value', cc.category, 'Active', cc.active)
      from case_category cc where cc.deleted_at is null order by cc.category ) jbo `).Row()
    err = row.Scan(&sjson)
    err = json.Unmarshal([]byte(sjson), &data)
    valueheader = "Category"
    title = "Manage Case Categories"
  } else {
    return nil, "", "", ErrInvalidLookupSettings
  }

  return data, title, valueheader, err
}

func JsonArrayGetDTOIdValues(tblname string) (data []map[string]interface{}, err error){
  var sjson string
  if(tblname == "cas_cat") {
    row := models.DBOrm.Raw(`SELECT json_agg(jsb.json_build_object) from (
      select json_build_object('category',cc.category) from case_category cc
      where cc.deleted_at is null ORDER BY cc.category ) jsb `).Row()
    err = row.Scan(&sjson)
  }
  if(err == nil) {
    err = json.Unmarshal([]byte(sjson), &data)
  }
  return data, err
}

func DTOIdValueCUD(action string, dto *DTOIdValue) (error)   {
  var err error
  if action == "insert" {
    dto.Uid = go_myutils.GetUUIDMD5Hash()
    if dto.Tbl == "cas_cat" {
      err = models.DBOrm.Exec("insert into case_category(uid, category, active) values (?, ?, ?)", dto.Uid, dto.Value, dto.Active).Error
    }
  } else if action == "update" {
    if dto.Tbl == "cas_cat" {
      err = models.DBOrm.Exec("update case_category set category = ?, active = ? where uid = ?", dto.Value, dto.Active, dto.Uid).Error
    }
  } else if action == "remove" {
    if dto.Tbl == "cas_cat" {
      err = models.DBOrm.Exec("update case_category set deleted_at = ? where uid = ?", time.Now(), dto.Uid).Error
    }
  }
  return err
}

func (pp *CaseCategory) BeforeCreate(scope *gorm.Scope) {
  scope.SetColumn("Uid", go_myutils.GetUUIDMD5Hash())
}
