package lookups

import (
	"bitbucket.org/jzmine/policeapp/models"
	"encoding/json"
	"bitbucket.org/jzmine/policeapp"
	"time"
	"github.com/garyburd/redigo/redis"
)


type NigState struct {
	Id int64 `gorm:"column:nig_state_id;primary_key"`
	StateName string
}

type Lga struct {
	Id int64 `gorm:"column:lga_id;primary_key"`
	LgaName string
}

func JsonArrayGetStateNames() (data []map[string]interface{},  err error) {
	rows, err := models.DBOrm.Table("nig_state").Select("state_name").Where("nig_state_id <= 38").Order("state_name").Rows()
	defer rows.Close()
	for rows.Next() {
		var statename string
		rows.Scan(&statename)
		data = append(data, map[string]interface{}{"statename":statename})
	}
	return
}

func JsonArrayGetLGANames(statename string) (data []map[string]interface{}) {
	rows, _ := models.DBOrm.Table("vw_state_lga").Select("lga_name").
		Where("state_name = ?", statename).Order("lga_name").Rows()
	defer rows.Close()
	var lganame string
	for rows.Next() {
		rows.Scan(&lganame)
		data = append(data, map[string]interface{}{"LgaName":lganame})
	}
	return
}

func JsonArrayStatesWithLGA() (data []map[string]interface{},  err error)  {
	/*
	sRows, err := models.DBOrm.Raw("SELECT nig_state_id, state_name from nig_state where nig_state_id <= 38 order by state_name").Rows()
	defer sRows.Close()
	for sRows.Next() {
		var statename string
		var stateid int64
		sRows.Scan(&stateid, &statename)

		var lgas []string
		var lganame string
		lRows, _ := models.DBOrm.Table("lga").Select("lga_name").Where("nig_state_id = ?", stateid).Order("lga_name").Rows()
		defer lRows.Close()
		for lRows.Next() {
			lRows.Scan(&lganame)
			lgas = append(lgas, lganame)
		}
		data = append(data, map[string]interface{}{"statename":statename, "lgas":lgas})
	}*/
	var sjson string
	statesLgaKey := "statesLga"
	if policeapp.PoliceCache.IsExist(statesLgaKey)  {
		sjson, _ = redis.String(policeapp.PoliceCache.Get(statesLgaKey), err)
	} else {
		row := models.DBOrm.Raw(`select json_agg(json_build_object('statename',sl.state_name, 'lgas', sl.lgas))
		FROM (
			select  sl.state_name, ARRAY(
			select s2.lga_name::text
			from vw_state_lga s2 where s2.state_name = sl.state_name
		) lgas
		from vw_state_lga sl
		group by 1
		order by 1
		) sl`).Row()
		err = row.Scan(&sjson)
		policeapp.PoliceCache.Put(statesLgaKey, sjson, 1*time.Hour)
	}
	json.Unmarshal([]byte(sjson), &data)
	return
}

func JsonArrayStatesWithLGA2() (data []map[string]interface{},  err error)  {
  var sjson string
  statesLgaKey := "statesLga2"
  if policeapp.PoliceCache.IsExist(statesLgaKey)  {
    sjson, _ = redis.String(policeapp.PoliceCache.Get(statesLgaKey), err)
  } else {
    row := models.DBOrm.Raw(`select json_agg(json_build_object('state',s.state_name,'lga',s.lga_name))
      from vw_state_lga s`).Row()
    err = row.Scan(&sjson)
    policeapp.PoliceCache.Put(statesLgaKey, sjson, 24*7*time.Hour)
  }
  json.Unmarshal([]byte(sjson), &data)
  return
}

func GetLGAId(lgaName string) (lgaId int64, err error) {
  var lga Lga
	err = models.DBOrm.Find(&lga, "lga_name = ?", lgaName).Error
  if err == nil {
    lgaId = lga.Id
  }
  return
}