package lookups

import (
	"github.com/jinzhu/gorm"
	"bitbucket.org/jzmine/policeapp/models"
	"bitbucket.org/jzmine/go_myutils"
)

type PfRanks struct {
	Id int64 `gorm:"column:pf_ranks_id;primary_key" json:"-"`
	RankName string
	RankCode string
	models.Model
}

func (pp *PfRanks) BeforeCreate(scope *gorm.Scope) {
	scope.SetColumn("Uid", go_myutils.GetUUIDMD5Hash())
}

func GetPoliceRankId(rname string) (id int64, err error)  {
	id, err = models.GetPkValueByField(&PfRanks{},"rank_name", rname)
	return
}

func JsonArrayGetRanks() (data []map[string]interface{}, err error)  {
	sRows, err := models.DBOrm.Raw("SELECT rank_name from pf_ranks order by 1").Rows()
	defer sRows.Close()
	var rank string
	for sRows.Next() {
		sRows.Scan(&rank)
		data = append(data, map[string]interface{}{"Rank":rank})
	}
	return
}