package cases

import (
  "time"
  "bitbucket.org/jzmine/policeapp/models"
  "bitbucket.org/jzmine/go_myutils"
  "github.com/jinzhu/gorm"
  "bitbucket.org/jzmine/policeapp/models/tables"
  "database/sql"
  "bitbucket.org/jzmine/policeapp/models/lookups"
  //"github.com/astaxie/beego/validation"
)

type Cases struct {
  Id int64 `gorm:"column:cases_id;primary_key"`
  DateIn time.Time
  TimeIn time.Time
  CaseFileNo string
  Title string
  Description string
  models.Model
}

func (c *Cases) BeforeCreate(scope *gorm.Scope) {
  scope.SetColumn("Uid", go_myutils.GetUUIDMD5Hash())
}

type DTOCases struct {
  Icategory []string `form:"icategory" valid:"required~Case Category is required"`
  Idatein string `form:"idatein"` //`form:"idatein"`
  Itimein string `form:"itimein"`
  Ititle string `form:"ititle" valid:"required~You must enter a Case Title"`
  Icaseno string `form:"icaseno" valid:"required~Case No is required"`
  Idescription string `form:"idescription" valid:"required~Please enter a Description for the Case"`
  Ioic string `form:"ioic" valid:"required~Please select an Office in Charge/Investigating Police Officer"`
  Uid string `form:"uid"`
  Icomplainants []string `form:"icomplainants"`
  Isuspects []string `form:"isuspects"`
  Ivictims []string `form:"ivictims"`
  DTOCaseDataIndividuals []DTOCaseDataIndividual `json:"-" form:"-"`
}

type DTOCaseDataIndividual struct {
  Csurname , Cfirstname, Cothernames,
  Cbvn, Cnimc, Csex, Clga, Cuid string
  Cdob string //models.CustomTime
  Cresaddr1, Cresaddr2, Cresaddrlga string
  Coffaddr1, Coffaddr2, Coffaddrlga string
  Ccontaddr1, Ccontaddr2, Ccontaddrlga string
  Cgsm1, Cgsm2, Cemail string
  Fpassport, Lpassport, Rpassport string
  IndividualType string
}

func CasesModelSaveCase(cm *DTOCases) (err error)  {
  var c Cases
  // TODO all records are reinserted
  /*if cm.Uid != "" {
    id, err := models.GetPkValue(&Cases{}, cm.Uid)
    if(err != nil) { return err   }
    err = models.DBOrm.Find(&c, id).Error
    if err != nil{ return err}
  }*/
  tx := models.DBOrm.Begin()
  err = func() error {
    if c.DateIn, err = time.Parse(models.CustomDateLayout, cm.Idatein); err != nil { return err  }
    if c.TimeIn, err = time.Parse(models.CustomTimeLayout, cm.Itimein); err != nil { return err  }
    c.Title = cm.Ititle
    c.CaseFileNo = cm.Icaseno
    c.Description = cm.Idescription
    c.Active = true

    if cm.Uid == "" {
      err = tx.Create(&c).Error
    } else {
      err = tx.Save(&c).Error
    }
    if err != nil { return err}

    //now save each catetory
    //lazy mans way: delete all existing 1st. this shouldn't be a problem since its not something we do always
    //TODO figure out a better way without deleting all
    tx.Exec(`delete from cases_case_category where cases_id = ?`, c.Id)
    var recId int64
    for _, category := range cm.Icategory {
      //id, err := models.GetPkValueByField()
      row := models.DBOrm.Raw(`select case_category_id from case_category where lower(category) = lower(?)`, category).Row()
      if err = row.Scan(&recId); err != nil { return err }
      if err = tx.Exec(`insert into cases_case_category (cases_id, case_category_id) values(?,?)`, c.Id, recId ).Error;
        err != nil { return err }
    }
    //same here too
    tx.Exec(`delete from cases_police_formation_officers where cases_id = ?`, c.Id)
    //var pftype string
    row := models.DBOrm.Raw(`select entities_id from dto_formation_police_officers
    where lower(fullnames) = lower(?)`, cm.Ioic).Row()
    if err = row.Scan(&recId); err != nil { return err }
    if err = tx.Exec(`insert into cases_police_formation_officers (cases_id, entities_id) values (?,?)`,
      c.Id, recId).Error; err != nil { return  err}
    //remove all entities for this case
    tx.Raw(`delete from cases_entities where cases_id = ?`, c.Id)

    //now save complainants
    for _, cmp := range cm.DTOCaseDataIndividuals {
      //var ent tables.Entities
      ent := tables.Entities{
        Model: models.Model{Active:true},
        EntityType: cmp.IndividualType,
        Persons: tables.Persons{
          Othernames: sql.NullString{String:cmp.Cothernames, Valid: true},
          Firstname: cmp.Cfirstname,
          BirthSex: cmp.Csex[0:1],
          Surname: cmp.Csurname,
          NimcNo: sql.NullString{String:cmp.Cnimc, Valid:true},
        },
        EntitiesContact: tables.EntitiesContact{
          Gsm_1: sql.NullString{String:cmp.Cgsm1, Valid:true},
          Gsm_2: sql.NullString{String:cmp.Cgsm2,Valid:true},
          Email_1: sql.NullString{String:cmp.Cemail,Valid:true},
        },
      }
      if cmp.Cbvn != "" {
        ent.Persons.Bvn = sql.NullString{String:cmp.Cbvn,Valid:true}
      }
      if cmp.Cdob != "" {
        if ent.Persons.Dob, err = time.Parse(models.CustomDateLayout, cmp.Cdob); err != nil { return err  }
      }
      var id int64
      if id, err = lookups.GetLGAId(cmp.Clga); err != nil { return err}
      ent.Persons.LgaId = sql.NullInt64{Int64: id, Valid:true}

      addr := tables.EntitiesAddress{
        AddrType: string('r'),
        Line_1: cmp.Cresaddr1,
        Line_2: sql.NullString{String:cmp.Cresaddr2, Valid: true},
      }
      if id, err = lookups.GetLGAId(cmp.Cresaddrlga); err != nil { return err}
      addr.LgaId = sql.NullInt64{Int64: id, Valid:true}
      ent.EntitiesAddress = append(ent.EntitiesAddress, addr)

      //now do we have off address
      if cmp.Coffaddr1 != "" || cmp.Coffaddr2 != "" || cmp.Coffaddrlga != ""{
        addr = tables.EntitiesAddress{
          AddrType: string('o'),
          Line_1: cmp.Cresaddr1,
          Line_2: sql.NullString{String:cmp.Coffaddr2, Valid: true},
        }
        if cmp.Coffaddrlga != "" {
          if id, err = lookups.GetLGAId(cmp.Coffaddrlga); err != nil { return err}
          addr.LgaId = sql.NullInt64{Int64: id, Valid:true}
        }
        ent.EntitiesAddress = append(ent.EntitiesAddress, addr)
      }
      //now do we have contact address
      if cmp.Ccontaddr1 != "" || cmp.Ccontaddr2 != "" || cmp.Ccontaddrlga != ""{
        addr = tables.EntitiesAddress{
          AddrType: string('c'),
          Line_1: cmp.Ccontaddr1,
          Line_2: sql.NullString{String:cmp.Ccontaddr2, Valid: true},
        }
        if cmp.Ccontaddrlga != "" {
          if id, err = lookups.GetLGAId(cmp.Ccontaddrlga); err != nil { return  err}
          addr.LgaId = sql.NullInt64{Int64: id, Valid:true}
        }
        ent.EntitiesAddress = append(ent.EntitiesAddress, addr)
      }

      if err = tx.Save(&ent).Error; err != nil { return err}
      //create an case entity
      if err = tx.Exec(`insert into cases_entities (cases_id, entities_id, entity_case_type) values (?,?,?)`,
        c.Id, ent.Id, cmp.IndividualType).Error; err != nil { return err}
      //save front passport
      if cmp.Fpassport != "" {
        if err = tx.Exec(`delete from entities_passport where file_uid = ?`, cmp.Fpassport).Error; err != nil { return err}
        if err = tx.Exec(`insert into entities_passport(entities_id, file_uid, side) values(?,?,?)`,
          ent.Id, cmp.Fpassport, "F").Error; err != nil { return err}
      }
      //save left passport
      if cmp.Lpassport != "" {
        if err = tx.Exec(`delete from entities_passport where file_uid = ?`, cmp.Lpassport).Error; err != nil { return err}
        if err = tx.Exec(`insert into entities_passport(entities_id, file_uid, side) values(?,?,?)`,
          ent.Id, cmp.Lpassport, "L").Error; err != nil { return err}
      }
      //save right passport
      if cmp.Rpassport != "" {
        if err = tx.Exec(`delete from entities_passport where file_uid = ?`, cmp.Rpassport).Error; err != nil { return err}
        if err = tx.Exec(`insert into entities_passport(entities_id, file_uid, side) values(?,?,?)`,
          ent.Id, cmp.Rpassport, "R").Error; err != nil { return err}
      }
    }


    return nil
  }()

  if err == nil {
    cm.Uid = c.Uid
    tx.Commit()
  } else {
    tx.Rollback()
  }

  return
}
