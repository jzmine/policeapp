package rbac

import (
  "github.com/mikespook/gorbac"
  "bitbucket.org/jzmine/policeapp/models"
  "fmt"
  "database/sql"
)

var (
  AppRbac  *gorbac.RBAC
)

func SetupRbac() (err error) {
  AppRbac = gorbac.New()

  //get all roles
  roles, err := models.DBOrm.Raw(`select rbac_roles_id, role_name from rbac_roles where deleted_at is null
    and parent_id is null `).Rows()
  defer roles.Close()

  for roles.Next() {
    var roleid int64
    var rolename string
    roles.Scan(&roleid, &rolename)
    //get all perms for this role
    var perms *sql.Rows
    perms, err = models.DBOrm.Raw(`select url, trim(both from action) from vw_roles_permissions
      where rbac_roles_id = ? `, roleid).Rows()
    if err == nil {
      defer perms.Close()
      rrole := gorbac.NewStdRole(rolename) //role only added if it has permissions
      for perms.Next() {
        var sperm, saction string
        perms.Scan(&sperm, &saction)
        //admin := NewLayerPermission("admin")
        //rperm := gorbac.NewStdPermission(sperm)
        // //admindashboard := NewLayerPermission("admin:dashboard")
        if(saction == "") {
          rperm := gorbac.NewLayerPermission(sperm+":r") //add the root permission as a read action
          rrole.Assign(rperm) //add d permission to the role
        } else {
          rpermact := gorbac.NewLayerPermission(fmt.Sprintf("%s:%s", sperm, saction))
          rrole.Assign(rpermact) //add d permission to the role
          /*if (canadd) {
            rpermcanadd := gorbac.NewLayerPermission(fmt.Sprintf("%s:add", sperm))
            rrole.Assign(rpermcanadd) //add d permission to the role
          }
          if (canread) {
            rpermcanread := gorbac.NewLayerPermission(fmt.Sprintf("%s:read", sperm))
            rrole.Assign(rpermcanread) //add d permission to the role
          }
          if (canupdate) {
            rpermcanupdate := gorbac.NewLayerPermission(fmt.Sprintf("%s:update", sperm))
            rrole.Assign(rpermcanupdate) //add d permission to the role
          }
          if (candelete) {
            rpermcandelete := gorbac.NewLayerPermission(fmt.Sprintf("%s:delete", sperm))
            rrole.Assign(rpermcandelete) //add d permission to the role
          }*/
        }
      }
      AppRbac.Add(rrole) //ok store it
    }
  }

  return
}