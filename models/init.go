package models

import (
	//"github.com/jmoiron/sqlx"
	"github.com/astaxie/beego/logs"
	"github.com/jinzhu/gorm"
	"time"
	"github.com/satori/go.uuid"
	"fmt"
)

//var DBHandle *sqlx.DB
var Log *logs.BeeLogger
var DBOrm *gorm.DB

type Model struct {
	Uid string
	Active bool
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
	DeletedAt *time.Time `json:"-"`
}

type CustomTime struct {
	time.Time
}

const CustomDateLayout = "2/Jan/2006"
const CustomTimeLayout = "3:4 PM"

func (ct *CustomTime) UnmarshalJSON(b []byte) (err error) {
	if b[0] == '"' && b[len(b)-1] == '"' {
		b = b[1 : len(b)-1]
	}
	ct.Time, err = time.Parse(CustomDateLayout, string(b))
	return
}

func (ct *CustomTime) MarshalJSON() ([]byte, error) {
	return []byte(ct.Time.Format(CustomDateLayout)), nil
}

/*
func (j CustomTime) Value() (driver.Value, error) {
  valueString, err := json.Marshal(j)
  return string(valueString), err
}

func (j *CustomTime) Scan(value interface{}) error {
  tm := value.(time.Time)
  byt := []byte(tm.Format(CustomTimeLayout))
  if err := json.Unmarshal(byt, &j); err != nil {
    return err
  }
  return nil
}
*/

func (model *Model) BeforeCreate(scope *gorm.Scope) {
	scope.SetColumn("Uid", uuid.NewV4())
}

func GetPkValue(model interface{}, uid string) (pkId int64, err error) {
	scope := DBOrm.NewScope(model)
	row := DBOrm.Table(scope.TableName()).Select(scope.PrimaryKey()).Where("uid = ?", uid).Row()
	err = row.Scan(&pkId)
	return
}

func GetUidValue(model interface{}, pkvalue int64) (uid string, err error)  {
	scope := DBOrm.NewScope(model)
	row := DBOrm.Table(scope.TableName()).Select("uid").Where(fmt.Sprintf("%s = ?",scope.PrimaryKey()),pkvalue).Row()
	err = row.Scan(&uid)
	return
}

func GetPkValueByField(model interface{}, whereFieldName string, whereFieldValue interface{}) (pkId int64, err error)  {
	scope := DBOrm.NewScope(model)
	row := DBOrm.Table(scope.TableName()).Select(scope.PrimaryKey()).Where(fmt.Sprintf("%s = ?", whereFieldName), whereFieldValue).Row()
	err = row.Scan(&pkId)
	return
}

type GridForeignKeyDatasource struct {
	Field string
	Value string
}

func GetGridForeignKeyDatasource(model interface{})  {

}

func init() {
	Log = logs.NewLogger(10000)
	Log.SetLogger("console", "")
	Log.EnableFuncCallDepth(true)
}
