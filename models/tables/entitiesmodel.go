package tables

import (
  "github.com/jinzhu/gorm"
  "bitbucket.org/jzmine/go_myutils"
  "bitbucket.org/jzmine/policeapp/models"
  "database/sql"
)

type EntitiesAddress struct {
  EntitiesId int64
  Line_1, AddrType string
  Line_2, Line_3 sql.NullString
  LgaId sql.NullInt64
  //Valid bool `gorm:"-" json:"-" form:"-"`
}

type EntitiesContact struct {
  EntitiesId int64
  Gsm_1, Email_1 sql.NullString
  Gsm_2, Email_2, Emergency_Phone_1 sql.NullString
}

type Entities struct {
  Id int64 `gorm:"column:entities_id;primary_key" json:"-"`
  EntityType string
  models.Model
  EntitiesAddress []EntitiesAddress
  EntitiesContact EntitiesContact
  PoliceOfficers PoliceOfficers
  Persons Persons
}

func (pp *Entities) BeforeCreate(scope *gorm.Scope) {
  scope.SetColumn("Uid", go_myutils.GetUUIDMD5Hash())
}