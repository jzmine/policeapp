package tables

type PersonsContact struct {
	PersonsId uint `json:"-"`
	Phone1 string
	Phone2 string
	Email1 string
	Email2 string
	EmergencyPhone1 string
}