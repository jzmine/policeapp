package tables

import (
	//"bitbucket.org/jzmine/policeapp/models"
	//"database/sql"
	//"bitbucket.org/jzmine/go_myutils"
	"github.com/jinzhu/gorm"
	//"github.com/satori/go.uuid"
	"bitbucket.org/jzmine/policeapp/models"
	"bitbucket.org/jzmine/go_myutils"
  "bitbucket.org/jzmine/policeapp"
  "github.com/garyburd/redigo/redis"
  "encoding/json"
)

type PersonFeaturesCategory struct {
	Id int64 `gorm:"column:person_features_category_id;primary_key" json:"-" `//
	models.Model
	Category string /*`db:"category"`*/
	Uid string //`gorm:"primary_key"`
	//Active bool
}
type PFCUid struct {
	PFCUid string `gorm:"column:uid"`
	Category string
}

func (fc *PersonFeaturesCategory) BeforeCreate(scope *gorm.Scope) {
	scope.SetColumn("Uid", go_myutils.GetUUIDMD5Hash())
}

func PersonFeaturesCategoryCRUD(action string, fc *PersonFeaturesCategory) (error) {
	var err error
	if action != "insert" {
		fc.Id, err = models.GetPkValue(&PersonFeaturesCategory{}, fc.Uid)
	}
	if action == "update" {
		//err = models.DBOrm.Save(&fc).Error
		//err = models.DBOrm.Model(&fc).Update(PersonFeaturesCategory{Active:fc.Active,Category:fc.Category}).Error
		err = models.DBOrm.Model(&fc).Updates(map[string]interface{}{"Active":fc.Active,"Category":fc.Category}).Error
		//err = models.DBOrm.Model(&fc).Update(map[string]interface{}{"active":fc.Active,"category":fc.Category}).Error
		//err = models.DBOrm.Save(&fc).Where("uid = ?", fc.Uid).Error
		//this.Data["json"] = rd.Value
	}else if (action == "insert") {
		//_, err = tables.FeatureCategoryInsert(&rd.Value)
		err = models.DBOrm.Create(&fc).Error
	} else if(action == "remove") {
		err = models.DBOrm.Delete(&fc).Error
	}
	return err
}

func JsonArrayPersonFeaturesCategory() (data []map[string]interface{},  err error)  {
	var sjson string
	ckey := policeapp.CacheKeyPersonFeatureCategory
	if policeapp.PoliceCache.IsExist(ckey)  {
		sjson, _ = redis.String(policeapp.PoliceCache.Get(ckey), err)
	} else {
		row := models.DBOrm.Raw(`SELECT json_agg(jsb.jb)
		from (
			select json_build_object('category', fc.category, 'feature', f.feature) jb
			from person_features f
			join person_features_category fc on f.person_features_category_id = fc.person_features_category_id
			where f.deleted_at is null and fc.deleted_at is NULL
			order by fc.category, f.feature
		) jsb`).Row()
		err = row.Scan(&sjson)
		policeapp.PoliceCache.Put(ckey, sjson, policeapp.CacheDuration)
	}
	json.Unmarshal([]byte(sjson), &data)
	return
}
/*
func FeatureCategoryGetAll() ([]FeatureCategory, error) {
	data := []FeatureCategory{}
	err := models.DBHandle.Select(&data, "select category, uid as id from Person_features_category")
	return data, err
}

func FeatureCategoryUpdate(data *FeatureCategory)	(sql.Result, error)  {
	result, err := models.DBHandle.NamedExec("update Person_features_category set category = :category where uid = :id", data)
	return result, err
}

func FeatureCategoryInsert(data *FeatureCategory) (sql.Result, error)  {
	data.Id = go_myutilsGetUUIDMD5Hash()
	println(data.Id)

	result, err := models.DBHandle.NamedExec("insert into Person_features_category (category, uid) values (:category, :id)",
		data)
	return result, err
}

*/