package tables

import (
	"bitbucket.org/jzmine/policeapp/models"
	"bitbucket.org/jzmine/go_myutils"
	"github.com/jinzhu/gorm"
)
//model_struct.go line 362
type PersonFeatures struct {
	Id int64 `gorm:"column:person_features_id;primary_key" json:"-" `
	PersonFeaturesCategory PersonFeaturesCategory `json:"-"` //`gorm:"ForeignKey:PersonFeatureCategoryId;AssociationForeignKey:Id"`
	//`gorm:"ForeignKey:PersonFeaturesCategoryID;AssociationForeignKey:Id"` ;AssociationForeignKey:Person_features_category_id
	PersonFeaturesCategoryId int64 `json:"-"` //`gorm:"column:person_features_category_id"`
	models.Model
	Feature string
	Uid string
	//Active bool
}

func (fc *PersonFeatures) BeforeCreate(scope *gorm.Scope) {
	scope.SetColumn("Uid", go_myutils.GetUUIDMD5Hash())
}

type DTOPersonFeatures struct {
	Uid string
	Feature string
	Active bool
	PFCUid string
}

func DTOPersonFeaturesGetAll() ([]DTOPersonFeatures, error) {
	var pfs []PersonFeatures
	err := models.DBOrm.Order("person_features_category_id, feature").Find(&pfs).Error
	if err != nil {
		return nil, err
	}

	var dto []DTOPersonFeatures
	var id string
	for _, pf := range pfs {
		id, err = models.GetUidValue(&PersonFeaturesCategory{}, pf.PersonFeaturesCategoryId)
		if(err != nil){return nil, err}
		dto = append(dto, DTOPersonFeatures{Uid: pf.Uid, Feature:pf.Feature, Active:pf.Active, PFCUid: id})
	}
	return dto, nil
}

func DTOPersonFeaturesCRUD(action string, dtoPC *DTOPersonFeatures) (error)   {
	/*{"value":{"Uid":"8640875bf106bfba9e8e2f09b2c03666","PFCUid":"11542d8a56decd28d6a867acd42952db","PFCUid_hidden":"Speaking","Feature":"Slurped Speech","Active":true,"Active_hidden":"Yes","ejControl_600":"edit","ejControl_601":"delete","ejControl_602":"save","ejControl_603":"cancel"},"action":"update","keyColumn":"Uid","key":"8640875bf106bfba9e8e2f09b2c03666","table":null,"params":{}}*/
	var err error
	var pf PersonFeatures
	if action != "insert" {
		pf.Id, err = models.GetPkValue(&PersonFeatures{}, dtoPC.Uid)
	}
	if action != "remove" {
		pf.Feature = dtoPC.Feature
		pf.Active = dtoPC.Active
		pf.PersonFeaturesCategoryId, err = models.GetPkValue(&PersonFeaturesCategory{}, dtoPC.PFCUid)
		if action == "insert" {
			err = models.DBOrm.Create(&pf).Error
		} else {
			err = models.DBOrm.Model(&pf).Update(map[string]interface{}{"Feature":dtoPC.Feature,"Active":dtoPC.Active,
				"PersonFeaturesCategoryId":pf.PersonFeaturesCategoryId}).Error
		}
	} else {
		err = models.DBOrm.Delete(&pf).Error
	}

	return err
}
/*
func PersonFeaturesCategoryCRUD(action string, fc *PersonFeaturesCategory) (error) {
	var err error
	if action != "insert" {
		fc.Id = models.GetPkValue("person_features_category","person_features_category_id", fc.Uid)
	}
	if action == "update" {
		//err = models.DBOrm.Save(&fc).Error
		err = models.DBOrm.Model(&fc).Update(PersonFeaturesCategory{Active:fc.Active,Category:fc.Category}).Error
		//err = models.DBOrm.Model(&fc).Update(map[string]interface{}{"active":fc.Active,"category":fc.Category}).Error
		//err = models.DBOrm.Save(&fc).Where("uid = ?", fc.Uid).Error
		//this.Data["json"] = rd.Value
	}else if (action == "insert") {
		//_, err = tables.FeatureCategoryInsert(&rd.Value)
		err = models.DBOrm.Create(&fc).Error
	} else if(action == "remove") {
		err = models.DBOrm.Delete(&fc).Error
	}
	return err
}
*/