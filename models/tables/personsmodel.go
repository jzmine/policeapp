package tables

import (
	//"bitbucket.org/jzmine/policeapp/models"
	"time"
	"database/sql"
	//"github.com/jinzhu/gorm"
	//"bitbucket.org/jzmine/go_myutils"
	//"bitbucket.org/jzmine/policeapp/models/setup"
)

type Persons struct {
	//Id int64 `gorm:"column:entities_id;primary_key" json:"-"`
	EntitiesId int64 //`gorm:"primary_key" json:"-"`
	Surname string
	Firstname string
	Othernames sql.NullString
	BirthSex string `gorm:"size:1"`
	Dob time.Time
	Bvn sql.NullString /*`gorm:"not null"`*/
	NimcNo sql.NullString
	LgaId sql.NullInt64
	//models.Model
	//PersonsContact PersonsContact
	//PoliceOfficers PoliceOfficers //`gorm:"ForeignKey:PersonsId"`
}

/*func (pp *Persons) BeforeCreate(scope *gorm.Scope) {
	scope.SetColumn("Uid", go_myutilsGetUUIDMD5Hash())
}*/

type PoliceOfficers struct {
	EntitiesId int64 //`gorm:"column:entities_id;primary_key" `
	PfType string `gorm:"size:1"`
	PfTypeId int64
	PoliceRanksId int64
}

func (PoliceOfficers) TableName() string  {
	return "persons_formation_officers"
}