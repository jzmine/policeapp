package policeapp

import "time"

var (
  CacheKeyPersonFeatureCategory string = "cKeyPersonFeatureCategory"
  CacheDuration time.Duration = time.Hour * 24 * 7 //7days
)
